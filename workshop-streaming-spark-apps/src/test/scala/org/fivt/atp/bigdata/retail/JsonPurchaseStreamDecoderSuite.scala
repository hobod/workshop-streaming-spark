package org.fivt.atp.bigdata.retail

import org.apache.spark.sql.DataFrame
import org.fivt.atp.bigdata.retail.json.PurchaseJson
import org.fivt.atp.bigdata.retail.model.{Item, Purchase}
import org.fivt.atp.bigdata.spark.SparkTestHarness
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class JsonPurchaseStreamDecoderSuite extends AnyFunSuite with Matchers with SparkTestHarness {
  import spark.implicits._

  private val jsonColumn = "json_purchases"

  test("decode JSON values") {
    val purchases = Seq(
      Purchase(
        timestamp = 1L,
        customerId = "customer1",
        items = Seq(Item("notebook", 100), Item("mouse", 20))
      ),
      Purchase(
        timestamp = 2L,
        customerId = "customer2",
        items = Seq(Item("phone", 50), Item("chair", 300))
      )
    )
    val data = toDF(purchases)

    val decoder = new JsonPurchaseStreamDecoder(jsonColumn)
    val result = decoder.decode(data)
      .collect()

    result should contain theSameElementsInOrderAs purchases
  }

  test("decode empty input") {
    val data = Seq.empty[String]
      .toDF(jsonColumn)

    val decoder = new JsonPurchaseStreamDecoder(jsonColumn)
    val result = decoder.decode(data)
      .collect()

    result shouldBe empty
  }

  test("the input data does not have the expected JSON column") {
    val purchases = Seq(
      Purchase(
        timestamp = 1L,
        customerId = "customer1",
        items = Seq(Item("notebook", 100), Item("mouse", 20))
      )
    )
    val data = toDF(purchases)

    val decoder = new JsonPurchaseStreamDecoder("expected_json_column")
    intercept[Exception] {
      decoder.decode(data)
    }
  }

  test("the expected JSON column has illegal name") {
    intercept[NullPointerException] {
      new JsonPurchaseStreamDecoder(null)
    }
    intercept[IllegalArgumentException] {
      new JsonPurchaseStreamDecoder("")
    }
  }

  private def toDF(purchases: Seq[Purchase]): DataFrame = {
    purchases
      .map(PurchaseJson.toJson)
      .toDF(jsonColumn)
  }

}
