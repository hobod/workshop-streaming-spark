package org.fivt.atp.bigdata.spark

import org.fivt.atp.bigdata.retail.json.ItemCountJson
import org.fivt.atp.bigdata.retail.model.{ItemCount, TimeWindow}
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class JsonStreamEncoderSuite extends AnyFunSuite with Matchers with SparkTestHarness {
  import spark.implicits._

  private val jsonColumn = "json_top_items"

  test("encode") {
    val itemCounts = Seq(
      ItemCount(timeWindow = TimeWindow(0L, 2000L), item = "a", count = 1),
      ItemCount(timeWindow = TimeWindow(1000L, 3000L), item = "b", count = 1)
    )
    val data = itemCounts.toDS()

    val encoder = new JsonStreamEncoder[ItemCount](jsonColumn)
    val result = encoder.encode(data)
      .select(jsonColumn)
      .as[String]
      .collect()
      .map(ItemCountJson.fromJson)
    result should contain theSameElementsInOrderAs itemCounts
  }

  test("encode empty input") {
    val data = Seq.empty[ItemCount]
      .toDS()

    val encoder = new JsonStreamEncoder[ItemCount](jsonColumn)
    val result = encoder.encode(data)
      .select(jsonColumn)
      .as[String]
      .collect()
    result shouldBe empty
  }

  test("JSON column has illegal name") {
    intercept[NullPointerException] {
      new JsonStreamEncoder[ItemCount](null)
    }
    intercept[IllegalArgumentException] {
      new JsonStreamEncoder[ItemCount]("")
    }
  }
}
