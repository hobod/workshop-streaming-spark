package org.fivt.atp.bigdata.spark

import org.apache.spark.sql.SparkSession

trait SparkTestHarness {

  protected implicit lazy val spark: SparkSession = SparkSession.builder()
    .master("local[4]")
    .appName(this.getClass.getSimpleName)
    .config("spark.ui.enabled", "false")
    .config("spark.sql.warehouse.dir", s"${sys.props("java.io.tmpdir")}/spark-warehouse")
    .config("spark.driver.bindAddress", "localhost")
    .getOrCreate()

}
