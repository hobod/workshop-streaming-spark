package org.fivt.atp.bigdata.retail.model

/**
 * Count of purchased items.
 *
 * @param timeWindow The time window when this count has been observed.
 * @param item       Item name.
 * @param count      How many items have been purchased.
 */
case class ItemCount(
    timeWindow: TimeWindow,
    item: String,
    count: Int
)
