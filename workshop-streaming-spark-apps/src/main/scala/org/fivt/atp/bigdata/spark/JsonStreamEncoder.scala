package org.fivt.atp.bigdata.spark

import org.apache.spark.sql.{DataFrame, Dataset}
import org.apache.spark.sql.functions.{col, struct, to_json}

/**
 * Implementation of [[StreamEncoder]] that supports JSON strings as the encoded format.
 *
 * @param jsonColumn Write JSON-encoded objects to this column in the output data set.
 */
class JsonStreamEncoder[T](jsonColumn: String) extends StreamEncoder[T] {
  require(jsonColumn.nonEmpty, "jsonColumn must be non-empty string")

  private val structColumn = "struct"

  override def encode(data: Dataset[T]): DataFrame = {
    val columns = data.columns
      .map(col)
    data
      .select(struct(columns: _*) as structColumn)
      .select(to_json(col(structColumn)) as jsonColumn)
  }

  override def encodedValueColumn: String = jsonColumn

}
