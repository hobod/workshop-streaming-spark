package org.fivt.atp.bigdata.retail.model

case class TimeWindow(
    from: Long,
    to: Long
)
