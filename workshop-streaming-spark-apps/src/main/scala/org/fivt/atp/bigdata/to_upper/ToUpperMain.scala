package org.fivt.atp.bigdata.to_upper

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, substring, upper}
import org.apache.spark.sql.streaming.{OutputMode, Trigger}
import org.apache.spark.sql.types.StringType
import org.fivt.atp.bigdata.config.AppConfigLoader
import org.fivt.atp.bigdata.spark.KafkaStreaming.{keyColumn, valueColumn}
import org.fivt.atp.bigdata.spark.{KafkaStreamReader, KafkaStreaming}
import org.slf4j.LoggerFactory

object ToUpperMain {

  private val logger = LoggerFactory.getLogger(this.getClass)

  def main(args: Array[String]): Unit = {
    implicit val spark: SparkSession = SparkSession.builder()
      .getOrCreate()

    val config = AppConfigLoader.appConfig
    logger.info(s"Config: $config")

    val inputValueColumn = "input_value"
    val inputStreamReader = new KafkaStreamReader(
      config.kafka.inputTopic,
      config.kafka.settings,
      inputValueColumn,
      startingOffsets = config.kafka.startingOffsets
    )
    val inputStream = inputStreamReader.readStream()

    val stringColumn = "string"
    val strings = inputStream
      .select(col(inputValueColumn) cast StringType as stringColumn)

    val upperCaseStringColumn = "upper_case_string"
    val upperCaseStrings = strings
      .select(upper(col(stringColumn)) as upperCaseStringColumn)

    val outputStream = upperCaseStrings
      .select(
        substring(col(upperCaseStringColumn), 0, 1) as keyColumn,
        col(upperCaseStringColumn) as valueColumn
      )

    val producerOptions = KafkaStreaming.withKafkaPrefix(config.kafka.settings)
    val checkpointLocation = s"${config.spark.checkpointDir}/${spark.conf.get("spark.app.name")}"
    val query = outputStream.writeStream
      .queryName(this.getClass.getSimpleName)
      .outputMode(OutputMode.Append())
      .trigger(Trigger.ProcessingTime(s"${config.spark.triggerInterval.toMillis} milliseconds"))
      .format("kafka")
      .options(producerOptions)
      .option("topic", config.kafka.outputTopic)
      .option("checkpointLocation", checkpointLocation)
      .start()
    query.awaitTermination()
  }

}
