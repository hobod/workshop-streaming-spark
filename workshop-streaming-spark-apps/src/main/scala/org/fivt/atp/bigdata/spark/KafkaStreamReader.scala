package org.fivt.atp.bigdata.spark

import org.apache.spark.sql.functions.col
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.fivt.atp.bigdata.spark.KafkaStreaming.valueColumn

class KafkaStreamReader(
    topic: String,
    consumerSettings: Map[String, String],
    outputValueColumn: String,
    startingOffsets: String
)(implicit spark: SparkSession) {

  def readStream(): DataFrame = {
    val consumerOptions = KafkaStreaming.withKafkaPrefix(consumerSettings)
    val input = spark.readStream
      .format("kafka")
      .options(consumerOptions)
      .option("subscribe", topic)
      .option("startingOffsets", startingOffsets)
      .load()

    input
      .select(col(valueColumn) as outputValueColumn)
  }

}
