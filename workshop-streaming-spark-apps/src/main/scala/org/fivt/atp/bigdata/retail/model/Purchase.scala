package org.fivt.atp.bigdata.retail.model

/**
 * Purchase fact.
 *
 * @param timestamp  Timestamp when the purchase took place.
 * @param customerId Identifier of the customer who made this purchase.
 * @param items      Purchased items.
 *                   We need Seq (though Iterable would suffice)
 *                   to ensure the compatibility with Spark SQL encoders.
 */
case class Purchase(
    timestamp: Long,
    customerId: String,
    items: Seq[Item]
) {

  def totalCost: Float = {
    items
      .map(_.price)
      .sum
  }

}
