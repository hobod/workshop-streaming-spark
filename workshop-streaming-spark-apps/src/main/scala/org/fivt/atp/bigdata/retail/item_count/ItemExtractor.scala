package org.fivt.atp.bigdata.retail.item_count

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.{col, explode}

trait ItemExtractor {

  protected def extractItems(purchases: DataFrame): DataFrame = {
    val items = purchases
      .select(
        col("timestamp"),
        explode(col("items")) as "item"
      )
      .select(
        col("timestamp"),
        col("item.name") as "name"
      )
    items
  }

}
