package org.fivt.atp.bigdata.retail

import org.apache.spark.sql.{DataFrame, Dataset}
import org.fivt.atp.bigdata.retail.model.Purchase

/**
 * Purchase stream decoder that decodes [[Purchase]] objects from a byte stream.
 * Implementations provide support for specific formats, e.g. Avro or JSON.
 */
trait PurchaseStreamDecoder {

  /**
   * Decodes purchases from a column containing binary data.
   *
   * @param data Input data.
   *             Must have the column specified by [[encodedValueColumn]]
   *             where encoded purchases are stored.
   * @return Data set of decoded purchases.
   */
  def decode(data: DataFrame): Dataset[Purchase]

  /**
   * Specifies the name of the column in the input data,
   * that contains the encoded purchases.
   *
   * @return Column name.
   */
  def encodedValueColumn: String

}
