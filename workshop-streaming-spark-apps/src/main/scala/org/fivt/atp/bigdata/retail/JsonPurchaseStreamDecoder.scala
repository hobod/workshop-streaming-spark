package org.fivt.atp.bigdata.retail

import org.apache.spark.sql.functions.{col, from_json}
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}
import org.fivt.atp.bigdata.retail.model.Purchase

/**
 * Implementation of [[PurchaseStreamDecoder]] that supports JSON strings as the encoded format.
 *
 * @param jsonColumn Read JSON-encoded purchases from this column in the input data.
 * @param spark      Spark session.
 */
class JsonPurchaseStreamDecoder(jsonColumn: String)(implicit spark: SparkSession)
  extends PurchaseStreamDecoder {
  require(jsonColumn.nonEmpty, "jsonColumn must be non-empty string")

  import spark.implicits._

  private val parsedJsonColumn = "parsed_json"

  private val purchaseSchema = spark.emptyDataset[Purchase]
    .schema

  override def decode(data: DataFrame): Dataset[Purchase] = {
    data
      .withColumn(parsedJsonColumn, from_json(col(encodedValueColumn) cast StringType, purchaseSchema))
      .select(s"$parsedJsonColumn.*")
      .as[Purchase]
  }

  override def encodedValueColumn: String = jsonColumn

}
