package org.fivt.atp.bigdata.config

import scala.concurrent.duration.FiniteDuration

case class AppConfig(
    kafka: Kafka,
    analytics: Analytics,
    spark: Spark
)

case class Kafka(
    inputTopic: String,
    outputTopic: String,
    settings: Map[String, String],
    startingOffsets: String
)

case class Analytics(
    topN: Int,
    windowSize: FiniteDuration,
    slideSize: FiniteDuration,
    delayThreshold: FiniteDuration
)

case class Spark(
    checkpointDir: String,
    triggerInterval: FiniteDuration
)
