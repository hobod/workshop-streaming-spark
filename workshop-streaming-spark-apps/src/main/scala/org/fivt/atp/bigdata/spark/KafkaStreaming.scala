package org.fivt.atp.bigdata.spark

object KafkaStreaming {

  /**
   * Data set column used for keys when reading/writing data from/to Kafka topics.
   * This value is specified in the Spark Structured Streaming + Kafka Integration Guide.
   */
  val keyColumn = "key"

  /**
   * Data set column used for values when reading/writing data from/to Kafka topics.
   * This value is specified in the Spark Structured Streaming + Kafka Integration Guide.
   */
  val valueColumn = "value"

  /**
   * Spark requires 'kafka.' prefix for Kafka producer or consumer settings.
   * For example: 'kafka.bootstrap.servers'.
   *
   * @param kafkaSettings Kafka settings.
   * @return Same settings, but names are prefixed.
   */
  def withKafkaPrefix(kafkaSettings: Map[String, String]): Map[String, String] = {
    kafkaSettings.map { case (k, v) =>
      s"kafka.$k" -> v
    }
  }

}
