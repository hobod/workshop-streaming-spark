package org.fivt.atp.bigdata.retail.model

case class Item(
    name: String,
    price: Float
)
