package org.fivt.atp.bigdata.spark

import org.apache.spark.sql.streaming.StreamingQuery

/**
 * Common treat for Spark Structured Streaming applications.
 */
trait StreamingApp {

  /**
   * Start the application and return the handler.
   *
   * @return Handle of the streaming query.
   */
  def start(): StreamingQuery

}
