package org.fivt.atp.bigdata.retail.item_count

import org.apache.spark.sql.functions.{col, count, struct, window}
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}
import org.fivt.atp.bigdata.retail.model.ItemCount
import org.fivt.atp.bigdata.spark.TimestampFunctions.timestampToEpochMillisUdf

import scala.concurrent.duration.FiniteDuration

class ItemCountCalculator(
    windowSize: FiniteDuration,
    slideSize: FiniteDuration
)(implicit spark: SparkSession) extends ItemExtractor {
  require(windowSize.length > 0, s"windowSize must be greater than zero $windowSize")
  require(slideSize.length > 0, s"slideSize must be greater than zero $slideSize")
  require(windowSize >= slideSize, s"slideSize: $slideSize must not exceed windowSize: $windowSize")

  import spark.implicits._

  def calcItemCounts(purchases: DataFrame): Dataset[ItemCount] = {
    val items = extractItems(purchases)

    val windowedItemCounts = items
      .groupBy(
        window(
          col("timestamp"),
          s"${windowSize.toSeconds} seconds",
          s"${slideSize.toSeconds} seconds"
        ),
        col("name")
      )
      .agg(count(col("name")) as "count")

    val itemCounts = windowedItemCounts
      .select(
        struct(
          timestampToEpochMillisUdf(col("window.start")) as "from",
          timestampToEpochMillisUdf(col("window.end")) as "to"
        ) as "timeWindow",
        col("name") as "item",
        col("count") cast IntegerType
      )
      .as[ItemCount]
    itemCounts
  }

}
