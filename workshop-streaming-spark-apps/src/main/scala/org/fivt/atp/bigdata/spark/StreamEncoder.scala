package org.fivt.atp.bigdata.spark

import org.apache.spark.sql.{DataFrame, Dataset}

trait StreamEncoder[T] {

  /**
   * Encodes a data set with domain-specific data to a column containing binary data.
   *
   * @param data Domain-specific data with schema defined by the domain model.
   * @return Data set with encoded data.
   *         The column with the encoded data is specified by [[encodedValueColumn]].
   */
  def encode(data: Dataset[T]): DataFrame

  /**
   * Specifies the name of the column in the output data,
   * that contains the encoded item counts.
   *
   * @return Column name.
   */
  def encodedValueColumn: String

}
