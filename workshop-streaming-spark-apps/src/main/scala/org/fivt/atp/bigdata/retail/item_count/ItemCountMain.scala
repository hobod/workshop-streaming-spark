package org.fivt.atp.bigdata.retail.item_count

import org.apache.spark.sql.SparkSession
import org.fivt.atp.bigdata.config.AppConfigLoader

object ItemCountMain {

  def main(args: Array[String]): Unit = {
    val config = AppConfigLoader.appConfig
    implicit val spark: SparkSession = SparkSession.builder()
      .getOrCreate()
    val app = new ItemCountApp(config)
    val query = app.start()
    query.awaitTermination()
  }

}
