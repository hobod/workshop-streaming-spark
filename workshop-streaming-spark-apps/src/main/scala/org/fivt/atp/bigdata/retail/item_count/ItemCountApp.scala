package org.fivt.atp.bigdata.retail.item_count

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.streaming.{OutputMode, StreamingQuery, Trigger}
import org.fivt.atp.bigdata.config.AppConfig
import org.fivt.atp.bigdata.retail.JsonPurchaseStreamDecoder
import org.fivt.atp.bigdata.retail.item_count.ItemCountApp._
import org.fivt.atp.bigdata.retail.model.ItemCount
import org.fivt.atp.bigdata.spark.KafkaStreaming.valueColumn
import org.fivt.atp.bigdata.spark.TimestampFunctions.epochMillisToTimestampUdf
import org.fivt.atp.bigdata.spark.{JsonStreamEncoder, KafkaStreamReader, KafkaStreaming, StreamingApp}
import org.slf4j.LoggerFactory

class ItemCountApp(config: AppConfig)(implicit spark: SparkSession) extends StreamingApp {

  private val logger = LoggerFactory.getLogger(this.getClass)

  override def start(): StreamingQuery = {
    logger.info(s"Config: $config")

    val purchaseStreamReader = new KafkaStreamReader(
      topic = config.kafka.inputTopic,
      consumerSettings = config.kafka.settings,
      outputValueColumn = jsonValueColumn,
      startingOffsets = config.kafka.startingOffsets
    )
    val encodedPurchases = purchaseStreamReader.readStream()

    val purchaseStreamDecoder = new JsonPurchaseStreamDecoder(jsonValueColumn)
    val decodedPurchases = purchaseStreamDecoder.decode(encodedPurchases)

    val purchases = decodedPurchases
      .withColumn("timestamp", epochMillisToTimestampUdf(col("timestamp")))
      .withWatermark("timestamp", s"${config.analytics.delayThreshold.toSeconds} seconds")
      .dropDuplicates("timestamp", "customerId")

    val itemCountCalculator = new ItemCountCalculator(
      windowSize = config.analytics.windowSize,
      slideSize = config.analytics.slideSize
    )
    val itemCounts = itemCountCalculator.calcItemCounts(purchases)

    val itemCountEncoder = new JsonStreamEncoder[ItemCount](valueColumn)
    val encodedItemCounts = itemCountEncoder.encode(itemCounts)

    val producerOptions = KafkaStreaming.withKafkaPrefix(config.kafka.settings)
    val checkpointLocation = s"${config.spark.checkpointDir}/${spark.conf.get("spark.app.name")}"
    val query = encodedItemCounts.writeStream
      .queryName(this.getClass.getSimpleName)
      .outputMode(OutputMode.Append())
      .trigger(Trigger.ProcessingTime(s"${config.spark.triggerInterval.toMillis} milliseconds"))
      .format("kafka")
      .options(producerOptions)
      .option("topic", config.kafka.outputTopic)
      .option("checkpointLocation", checkpointLocation)
      .start()
    query
  }

}

object ItemCountApp {
  private val jsonValueColumn = "json_value"
}
