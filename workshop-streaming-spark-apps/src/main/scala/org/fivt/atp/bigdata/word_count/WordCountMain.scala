package org.fivt.atp.bigdata.word_count

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.streaming.{OutputMode, Trigger}
import org.apache.spark.sql.types.StringType
import org.fivt.atp.bigdata.config.AppConfigLoader
import org.fivt.atp.bigdata.spark.{KafkaStreamReader, KafkaStreaming}
import org.slf4j.LoggerFactory

object WordCountMain {

  private val logger = LoggerFactory.getLogger(this.getClass)

  def main(args: Array[String]): Unit = {
    implicit val spark: SparkSession = SparkSession.builder()
      .getOrCreate()

    val config = AppConfigLoader.appConfig
    logger.info(s"Config: $config")

    val inputValueColumn = "input_value"
    val inputStreamReader = new KafkaStreamReader(
      config.kafka.inputTopic,
      config.kafka.settings,
      inputValueColumn,
      startingOffsets = config.kafka.startingOffsets
    )
    val inputStream = inputStreamReader.readStream()

    val stringColumn = "string"
    val strings = inputStream
      .select(col(inputValueColumn) cast StringType as stringColumn)

    val wordColumn = "word"
    val words = strings
      .select(
        explode(
          split(
            regexp_replace(lower(col(stringColumn)), "[^\\w]", " "),
            "\\s+"
          )
        ) as wordColumn
      )
      .where(length(trim(col(wordColumn))) gt 0)

    val wordCountColumn = "word_count"
    val wordCounts = words
      .groupBy(wordColumn)
      .agg(count(wordColumn) as wordCountColumn)

    val outputStream = wordCounts
      .select(
        substring(col(wordColumn), 0, 1) as KafkaStreaming.keyColumn,
        concat_ws(":", col(wordColumn), col(wordCountColumn)) as KafkaStreaming.valueColumn
      )

    val producerOptions = KafkaStreaming.withKafkaPrefix(config.kafka.settings)
    val checkpointLocation = s"${config.spark.checkpointDir}/${spark.conf.get("spark.app.name")}"
    val query = outputStream.writeStream
      .queryName(this.getClass.getSimpleName)
      .outputMode(OutputMode.Complete())
      .trigger(Trigger.ProcessingTime(s"${config.spark.triggerInterval.toMillis} milliseconds"))
      .format("kafka")
      .options(producerOptions)
      .option("topic", config.kafka.outputTopic)
      .option("checkpointLocation", checkpointLocation)
      .start()
    query.awaitTermination()
  }

}
