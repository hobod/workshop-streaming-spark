package org.fivt.atp.bigdata.spark

import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.functions.udf

import java.sql.Timestamp

object TimestampFunctions {

  val epochMillisToTimestampUdf: UserDefinedFunction = udf { epochMillis: Long =>
    new Timestamp(epochMillis)
  }

  val timestampToEpochMillisUdf: UserDefinedFunction = udf { ts: Timestamp =>
    ts.getTime
  }

}
