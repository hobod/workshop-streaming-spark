package org.fivt.atp.bigdata.retail.json

import org.fivt.atp.bigdata.retail.model.Purchase
import org.json4s.jackson.Serialization
import org.json4s.jackson.Serialization.write
import org.json4s.{Formats, NoTypeHints}

object PurchaseJson {

  private implicit val formats: AnyRef with Formats = Serialization.formats(NoTypeHints)

  def toJson(purchase: Purchase): String = {
    write(purchase)
  }

}
