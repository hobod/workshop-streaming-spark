package org.fivt.atp.bigdata.config

import pureconfig.ConfigSource
import pureconfig.generic.auto.exportReader

object AppConfigLoader {

  private val namespace = "app"

  def appConfig: AppConfig = {
    ConfigSource.default
      .at(namespace)
      .loadOrThrow[AppConfig]
  }

}
