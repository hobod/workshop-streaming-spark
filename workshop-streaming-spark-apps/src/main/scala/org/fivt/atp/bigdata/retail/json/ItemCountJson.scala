package org.fivt.atp.bigdata.retail.json

import org.fivt.atp.bigdata.retail.model.ItemCount
import org.json4s.DefaultFormats
import org.json4s.jackson.JsonMethods.parse

object ItemCountJson {

  private implicit val formats: DefaultFormats.type = DefaultFormats

  def fromJson(json: String): ItemCount = {
    parse(json)
      .extract[ItemCount]
  }

}
