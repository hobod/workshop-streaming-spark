# Playground project for Apache Spark Structured Streaming

[![Main](https://github.com/tashoyan/workshop-streaming-spark/actions/workflows/main.yml/badge.svg)](https://github.com/tashoyan/workshop-streaming-spark/actions/workflows/main.yml)

This project contains simple examples of streaming data processing
using [Apache Spark Structured Streaming](https://spark.apache.org/docs/latest/structured-streaming-programming-guide.html).
The code is written in [Scala](https://www.scala-lang.org/) programming language
The project uses [Maven](https://maven.apache.org/) as a build tool.
The Spark applications run in [YARN](https://hadoop.apache.org/docs/current/hadoop-yarn/hadoop-yarn-site/YARN.html).
All streaming applications use [Kafka](https://kafka.apache.org/) to consume input data and write results.

## List of applications

The `bin/` directory of the distribution contains launchers for the following applications:

1. `dummy-submit.sh` - a dummy application.
   Not actually a streaming application, just a dummy Spark program.
   The purpose is to check that everything is correctly installed and runnable.
   
1. `to-upper-submit.sh` - converts a stream of strings to upper.

1. `word-count-submit.sh` - running word count.

1. `item-count-submit.sh` - item count in time windows.
   This application implements a use-case from retail:
   analyse a stream of purchases and count how many items have been sold
   within each time window.

The `workshop-streaming-spark-apps` sub-module contains Spark applications,
as well as unit tests.

The `workshop-streaming-spark` sub-module packages the distribution archive.
It also provides default configuration files and application launchers.
