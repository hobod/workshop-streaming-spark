# Checkpoints and recovery after a failure

Let's kill a running application and see how it recovers its state from the checkpoint.

1. Start the word count application

   ```bash
   ./workshop-streaming-spark/bin/word-count-submit.sh
   ```
   Open YARN UI in a browser and wait until the application comes to the RUNNING state.

1. Start Kafka console producer and consumer in their separate terminals

1. Send some trivial text to the producer. See the word counts on the consumer.

1. Kill the application

1. Start the application again

1. Send the same text as previously to the producer

   The word counts continue incrementing despite the application restart.

1. Kill the application

1. Delete the checkpoints

   The checkpoints directory is configured in the application's configuration file: `app.conf`.
   Delete this directory:
   ```bash
   hdfs dfs -rm -r checkpoints
   ```

1. Start the application again

1. Send the same text as previously to the producer

   This time the word counts have been reset.

1. Change the number of partitions in Kafka topics

  The application restarts without problems.

1. Change the value of `spark.sql.shuffle.partitions` from `2` to `4`

  The application restarts without problems.
  However, we can see a warning in the log:
  ```text
  org.apache.spark.sql.execution.streaming.OffsetSeqMetadata  - Updating the value of conf 'spark.sql.shuffle.partitions' in current session from '4' to '2'.
  ```

1. Change the input topic

  Set to a dummy value.
  The application fails to restart.
