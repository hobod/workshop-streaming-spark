# Kafka Cheatsheet

Useful commands with Kafka console utilities

List my Kafka topics
```shell
kafka-topics --bootstrap-server sber-node01.atp-fivt.org:9092 --list | grep $USER
```

Create topics
```shell
kafka-topics \
  --bootstrap-server sber-node01.atp-fivt.org:9092 \
  --create \
  --replication-factor 1 \
  --partitions 2 \
  --topic workshop-streaming-input-$USER
kafka-topics \
  --bootstrap-server sber-node01.atp-fivt.org:9092 \
  --create \
  --replication-factor 1 \
  --partitions 2 \
  --topic workshop-streaming-output-$USER
```

Describe topics
```shell
kafka-topics \
  --bootstrap-server sber-node01.atp-fivt.org:9092 \
  --describe \
  --topic workshop-streaming-input-$USER
kafka-topics \
  --bootstrap-server sber-node01.atp-fivt.org:9092 \
  --describe \
  --topic workshop-streaming-output-$USER
```

Consume from a topic
```shell
kafka-console-consumer \
  --bootstrap-server sber-node01.atp-fivt.org:9092 \
  --topic workshop-streaming-output-$USER
```

Produce to a topic
```shell
kafka-console-producer \
  --broker-list sber-node01.atp-fivt.org:9092 \
  --topic workshop-streaming-input-$USER
```

Change partition number for topics
```shell
kafka-topics \
  --bootstrap-server sber-node01.atp-fivt.org:9092 \
  --alter \
  --partitions 3 \
  --topic workshop-streaming-input-$USER
kafka-topics \
  --bootstrap-server sber-node01.atp-fivt.org:9092 \
  --alter \
  --partitions 3 \
  --topic workshop-streaming-output-$USER
```

Delete topics
```shell
kafka-topics \
  --bootstrap-server sber-node01.atp-fivt.org:9092 \
  --delete \
  --topic workshop-streaming-input-$USER
kafka-topics \
  --bootstrap-server sber-node01.atp-fivt.org:9092 \
  --delete \
  --topic workshop-streaming-output-$USER
```
