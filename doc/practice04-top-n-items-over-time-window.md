# Calculate Top N items in time windows

This application implements another use case from online retail.
The demand on certain goods may rapidly change during a day.
We want to detect on the fly the most popular items purchased by our customers.
This would allow us to proactively supply the goods and avoid the out-of-stock situation.

Let's start with a simplified problem:
- Finite (not streaming) data set
- N = 2

The input of purchased items looks like:
```text
+-----------+--------+
|hour       |item    |
+-----------+--------+
|09:00-10:00|notebook|
|09:00-10:00|mouse   |
|09:00-10:00|notebook|
|09:00-10:00|umbrella|
|09:00-10:00|notebook|
|09:00-10:00|mouse   |
|09:30-10:30|shoes   |
|09:30-10:30|umbrella|
|09:30-10:30|umbrella|
|09:30-10:30|shirt   |
|09:30-10:30|shirt   |
|09:30-10:30|shirt   |
+-----------+--------+
```
We put aside the necessary transformations to extract items from purchase transactions. We also assume that the input stream is already partitioned by time windows.

Or goal is to get the following result:
```text
+-----------+--------------+
|hour       | top_items    |
+-----------+--------------+
|09:00-10:00|notebook,mouse|
|09:30-10:30|shirt,umbrella|
+-----------+--------------+
```

1. Start Spark Shell

   On the edge node:
   ```bash
   SPARK_DIST_CLASSPATH="$(hadoop classpath)" /opt/spark/latest/bin/spark-shell
   ```

1. Create the data set of purchases

   In Spark Shell:
   ```scala
   Seq(
     ("09:00-10:00", Seq("notebook", "mouse")),
     ("09:00-10:00", Seq("notebook", "umbrella")),
     ("09:00-10:00", Seq("notebook", "mouse")),
     ("09:30-10:30", Seq("shoes", "umbrella")),
     ("09:30-10:30", Seq("umbrella", "shirt")),
     ("09:30-10:30", Seq("shirt"))
   )
   .toDF("hour", "items")
   .createOrReplaceTempView("purchases")
   ```

   Explore the data set of purchases:
   ```scala
   sql("SELECT * FROM purchases").show
   ```

   **Now think: How to get the data set of items?**

   ```scala
   sql("SELECT hour, explode(items) AS item FROM purchases").createOrReplaceTempView("items")
   ```

1. Create the data set of items

   In Spark Shell:
   ```scala
   Seq(
     ("09:00-10:00", "notebook"),
     ("09:00-10:00", "mouse"),
     ("09:00-10:00", "notebook"),
     ("09:00-10:00", "umbrella"),
     ("09:00-10:00", "notebook"),
     ("09:00-10:00", "mouse"),
     ("09:30-10:30", "shoes"),
     ("09:30-10:30", "umbrella"),
     ("09:30-10:30", "umbrella"),
     ("09:30-10:30", "shirt"),
     ("09:30-10:30", "shirt"),
     ("09:30-10:30", "shirt"),
   )
   .toDF("hour", "item")
   .createOrReplaceTempView("items")
   ```

   Explore the data set of items:
   ```scala
   sql("SELECT * FROM items").show
   ```

   **Now think: How to calculate Top 2 items for every hour?**

   * How many aggregation operations we need?
   * Which kind of aggregations we need?
   * Are these operations supported in Spark Structured Streaming?

1. Calculate `item_counts` - item counts for each hour

   ```scala
   sql("SELECT hour, item, count(item) as count FROM items GROUP BY hour, item").createOrReplaceTempView("item_counts")
   ```

   Explore `item_counts`:
   ```scala
   sql("SELECT * FROM item_counts ORDER BY hour, count desc").show
   ```

1. Rank items by their counts

   ```scala
   sql("SELECT hour, item, count, row_number() OVER (PARTITION BY hour ORDER BY count DESC) as rn FROM item_counts").createOrReplaceTempView("ranked_counts")
   ```

   Explore `ranked_counts`:
   ```scala
   sql("SELECT * FROM ranked_counts").show
   ```

1. Get Top 2 items

   ```scala
   sql("SELECT hour, collect_list(item) as top_items FROM ranked_counts WHERE rn <= 2 GROUP BY hour").createOrReplaceTempView("top_items")
   ```

   Explore `top_items`:
   ```scala
   sql("SELECT * FROM top_items ORDER by hour").show
   ```

---

Open `spark-catalyst/UnsupportedOperationChecker.scala` from Spark source code.
```scala
if (aggregates.size > 1) {
  throwError(
    "Multiple streaming aggregations are not supported with " +
      "streaming DataFrames/Datasets")(plan)
}
...
case Window(_, _, _, child) if child.isStreaming =>
  throwError("Non-time-based windows are not supported on streaming DataFrames/Datasets")
```


