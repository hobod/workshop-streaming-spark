# Building, deploying and running the applications

## Build, run unit tests, package

Run a Maven command:
```shell
mvn clean install
```
The command will compile the Scala code, run unit tests and assemble the distribution.
The distribution will be generated here:
```
workshop-streaming-spark/target/workshop-streaming-spark-<version>.zip
```

## Deploy and run

1. Copy the distribution to an edge node of your Hadoop cluster.
   This node should have connectivity to HDFS and YARN.
   ```shell
   scp workshop-streaming-spark/target/*.zip <username>@<edgenode hostname>:
   ```

1. Login to the edge node:
   ```shell
   ssh <username>@<edgenode hostname>
   ```
   
1. Unpack the distribution:
   ```shell
   test -d workshop-streaming-spark && \
     rm -r workshop-streaming-spark ; \
     unzip workshop-streaming-spark-*.zip
   ```

1. Run the dummy application:
   ```shell
   ./workshop-streaming-spark/bin/dummy-submit.sh
   ```
   
1. Go to Spark UI and see the application progress
   The dummy Spark application will run for a few minutes.

## Cleanup

**Important: Don't forget to kill your streaming applications in YARN.**

A streaming applications is intended to run forever. It will never end by itself.
```bash
yarn application -kill <YARN app id>
```
