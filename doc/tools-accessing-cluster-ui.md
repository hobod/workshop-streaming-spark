# Accessing web UI of the cluster from your browser

The cluster resides in a protected network inaccessible from Internet.
In order to access the UI of YARN, Spark, History Server and other services,
you have to configure a SOCKS5 proxy with dynamic SSH port forwarding.

## Setting up SSH port forwarding and SOCKS5 proxy

Execute the following command:
```shell
ssh -N -D 8124 <username>@sber-client.atp-fivt.org
```
This command sets a SOCKS5 proxy on the port 8124 of your local machine.
Every connection to this port will be forwarded to the remote machine sber-client.atp-fivt.org.
You need a valid login on the remote machine.

## Configuring your browser to use the proxy

You have to use the proxy auto-config (PAC) file: `realtime-bigdata-workshop.pac`.
This PAC script tells your browser to use the SOCKS5 proxy when accessing the services in the cluster.

In the browser settings choose: Network Settings -> Proxy -> Automatic proxy configuration URL.
Put the path to the PAC script: `file:///path/to/workspace/realtime-bigdata-workshop.pac`.

For example:
`file:///c:/workspace/workshop-streaming-spark/realtime-bigdata-workshop.pac`
