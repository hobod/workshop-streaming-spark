# Count purchased items in time windows

This application illustrates a use case from online retail.
The goal is to monitor the change in the demand: which items customers purchase more frequently?

We receive a stream of purchases made on a web-site.
From each purchase fact we extract the purchased items.
We count items within time windows.

For example, we receive on a stream of purchases:
```text
+----------------------------------------------------+
| timestamp | customer  | items                      |
+----------------------------------------------------+
| 09:15     | John      | umbrella, shoes, sunscreen |
| 09:45     | Alice     | notebook, monitor          |
| 10:20     | Krzysztof | mouse, monitor, shirt      |
| ... 	    | ...       | ...                        |
```
We produce on output a stream of time-windowed item counts:
```text
+---------------------------------+
| interval    | item      | count |
+---------------------------------+
| 09:00-10:00 | umbrella  |  1250 |
| 09:00-10:00 | sunscreen |  2343 |
| 09:00-10:00 | monitor   |     2 |
| 09:30-10:30 | umbrella  |     5 |
| 09:30-10:30 | mask      | 25800 |
| 09:30-10:30 | monitor   |   520 |
| 10:00-10:30 | ...       |   ... |
| ...         | ...       |   ... |
```

1. Become familiar with the data model

   - Purchases: class `retail.model.Purchases`
   - Item counts: class `retail.model.ItemCount`

1. We use JSON strings as a communication protocol

   On input - Purchase:
   ```json
   {
     "timestamp": 1617486862112,
     "customerId": "customer1",
     "items": [
       {
         "name": "notebook",
         "price": 100.0
       },
       {
         "name": "mouse",
         "price": 20.0
       }
     ]
   }
   ```

   On output - Item count:
   ```json
   {
     "timeWindow": {
       "from": 1617486860000,
       "to": 1617486870000
     },
     "item": "notebook",
     "count": 2
   }
   ```

1. Read the code

   Classes in the package `retail.item_count`

1. Deploy the application on the edge node

1. Review the application configuration

   ```bash
   vim ./workshop-streaming-spark/conf/app.conf
   ```

   Configure non-overlapping time windows (also known as "tumbling windows").
   Configure delay threshold for watermarks.
   ```conf
   analytics {
     window-size = "10 sec"
     slide-size = "10 sec"
     delay-threshold = "5 sec"
   }
   ```

1. Run Kafka console consumer on the output topic

   In a separate console on the edge node:
   ```bash
   kafka-console-consumer \
     --bootstrap-server sber-node01.atp-fivt.org:9092 \
     --topic workshop-streaming-output-$USER   
   ```

1. Run Kafka console producer on the input topic

   In a separate console on the edge node:
   ```bash
   kafka-console-producer \
     --broker-list sber-node01.atp-fivt.org:9092 \
     --topic workshop-streaming-input-$USER
   ```

1. Start the item count application

   ```bash
   ./workshop-streaming-spark/bin/item-count-submit.sh
   ```
   Open YARN UI in a browser and wait until the application comes to the RUNNING state.

1. Send bunch #1 of purchases

   Copy the following JSON lines to the Kafka console producer:
   ```text
   {"timestamp":10000,"customerId":"customer1","items":[{"name":"notebook","price":100.0},{"name":"mouse","price":20.0}]}
   {"timestamp":15000,"customerId":"customer1","items":[{"name":"notebook","price":100.0},{"name":"chair","price":80.0}]}
   {"timestamp":19000,"customerId":"customer1","items":[{"name":"notebook","price":100.0},{"name":"mouse","price":20.0}]}
   {"timestamp":25001,"customerId":"customer1","items":[{"name":"umbrella","price":50.0},{"name":"shirt","price":50.0}]}
   ```
   Look at the Kafka console consumer and see the item counts.

   Questions:
   - Do we see all the items we have sent? Why?
   - What should we do to see the remaining items?
   - What is the current watermark value?

1. Send bunch #2 of purchases

   Copy the following JSON lines to the Kafka console producer:
   ```text
   {"timestamp":15000,"customerId":"customer1","items":[{"name":"chair","price":80.0},{"name":"mouse","price":20.0}]}
   {"timestamp":23000,"customerId":"customer1","items":[{"name":"notebook","price":100.0},{"name":"mouse","price":20.0}]}
   {"timestamp":35001,"customerId":"customer1","items":[{"name":"mouse","price":20.0},{"name":"shirt","price":50.0}]}
   ```
   Look at the Kafka console consumer and see the item counts.

   Questions:
   - Identify: which purchases from bunches #1 and #2 have been processed? Why?
   - The bunch #2 has a purchase at timestamp 15000 - why we don't see the corresponding update in item counts for the time window [10000, 20000)?

1. Kill the application

   Kill the application in YARN:
   ```bash
   yarn application -kill <YARN app id>
   ```
   Cleanup checkpoints:
   ```bash
   hdfs dfs -rm -r checkpoints
   ```

1. Change the output mode to Update

   Edit the class ItemCountApp - change the output mode to `OutputMode.Update`.

1. Rebuild-redeploy-start

   On your local machine:
   ```bash
   mvn clean install
   scp workshop-streaming-spark/target/*.zip <username>@<edgenode hostname>:
   ```
   On the edge node:
   ```bash
   test -d workshop-streaming-spark && \
     rm -r workshop-streaming-spark ; \
     unzip workshop-streaming-spark-*.zip
   ./workshop-streaming-spark/bin/item-count-submit.sh
   ```

1. Send again bunch #1 of purchases

   Copy the following JSON lines to the Kafka console producer:
   ```text
   {"timestamp":10000,"customerId":"customer1","items":[{"name":"notebook","price":100.0},{"name":"mouse","price":20.0}]}
   {"timestamp":15000,"customerId":"customer1","items":[{"name":"notebook","price":100.0},{"name":"chair","price":80.0}]}
   {"timestamp":19000,"customerId":"customer1","items":[{"name":"notebook","price":100.0},{"name":"mouse","price":20.0}]}
   {"timestamp":25001,"customerId":"customer1","items":[{"name":"umbrella","price":50.0},{"name":"shirt","price":50.0}]}
   ```
   Look at the Kafka console consumer and see the item counts.

   Questions:
   - Compare the item counts produced for the purchases bunch #1: `OutputMode.Update` vs `OutputMode.Append`. Explain the difference.

1. Continue playing with the item count application

1. Finally, cleanup.

   Kill your application in YARN.
