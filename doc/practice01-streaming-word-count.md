# Streaming word count

This application implements a running word count on a stream of strings.

We receive the input stream from an input Kafka topic.
We send the stream of word counts to the output Kafka topic.

1. Read the code

   Class `word_count.WordCountMain`

1. Deploy the application on the edge node

1. Run Kafka console consumer on the output topic

   In a separate console on the edge node:
   ```bash
   kafka-console-consumer \
     --bootstrap-server sber-node01.atp-fivt.org:9092 \
     --topic workshop-streaming-output-$USER   
   ```

1. Run Kafka console producer on the input topic

   In a separate console on the edge node:
   ```bash
   kafka-console-producer \
     --broker-list sber-node01.atp-fivt.org:9092 \
     --topic workshop-streaming-input-$USER
   ```

1. Start the word count application

   ```bash
   ./workshop-streaming-spark/bin/word-count-submit.sh
   ```
   Open YARN UI in a browser and wait until the application comes to the RUNNING state.

1. Send the text #1

   Copy the following text to the Kafka console producer:
   ```text
   To be, or not to be, that is the question:
   Whether 'tis nobler in the mind to suffer
   The slings and arrows of outrageous fortune
   ```
   Look at the Kafka console consumer and see the word counts.

1. Send the text #2

   ```text
   He wore the shirt and the trousers I had given him and he sill had the outrageous hat perched on his head
   ```
   Look at the Kafka console consumer and see the word counts.

   Questions:
   - Why we get all counters, even if they haven't been updated?

1. Kill the application

   Kill the application in YARN:
   ```bash
   yarn application -kill <YARN app id>
   ```
   Cleanup checkpoints:
   ```bash
   hdfs dfs -rm -r checkpoints
   ```

1. Change the output mode to Update

   Edit the class WordCountMain - change the output mode to `OutputMode.Update`.

1. Rebuild-redeploy-start

   On your local machine:
   ```bash
   mvn clean install
   scp workshop-streaming-spark/target/*.zip <username>@<edgenode hostname>:
   ```
   On the edge node:
   ```bash
   test -d workshop-streaming-spark && \
     rm -r workshop-streaming-spark ; \
     unzip workshop-streaming-spark-*.zip
   ./workshop-streaming-spark/bin/word-count-submit.sh
   ```

1. Send again the text #1

   Copy the following text to the Kafka console producer:
   ```text
   To be, or not to be, that is the question:
   Whether 'tis nobler in the mind to suffer
   The slings and arrows of outrageous fortune
   ```
   Look at the Kafka console consumer.

1. Send again the text #2

   Copy the following text to the Kafka console producer:
   ```text
   He wore the shirt and the trousers I had given him and he sill had the outrageous hat perched on his head
   ```
   Look at the Kafka console consumer and see the word counts.

   Questions:
   - Compare the word counts: `OutputMode.Update` vs `OutputMode.Complete`. Explain the difference.

1. Continue playing with the word count application

1. Finally, cleanup.

   Kill your application in YARN.
