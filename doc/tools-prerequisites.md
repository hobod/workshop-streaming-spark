# Prerequisites for the workshop

## Software on your machine

### Bash terminal

* Linux or Mac - just terminal
* Windows - one of:
  * GitBash
  * Windows Subsystem for Linux (WSL)
  
### Git

* Linux: install using the package manager from your Linux distribution
* Mac: install using Brew
* Windows - GitBash (WSL - same as on Linux)

```shell
git --version
```

### JDK 8 or later

* OpenJDK of the latest version is preferred
* Make sure you have `javac` in your $PATH

```shell
javac -version
```

### Maven 3.6.0 or later

* Make sure you have `mvn` in your $PATH

```shell
mvn --version
```

### IntelliJ IDEA

* Download for your OS
* Enable Scala plugin

## Configuration on the Hadoop cluster edge node

Edge node - the node that has the access to YARN, HDFS and other Hadoop cluster resources.
Typically, you submit a Spark application from an edge node.

### Spark

By default, the Spark distribution is expected here:
`/opt/spark/latest`.
To use another path, configure the environment variable `SPARK_HOME`
in the file `conf/env.sh`.

### Hadoop configuration

By default, the directory with Hadoop configuration files is expected here:
`/etc/hadoop/conf`.
To use another path, configure the environment variable `HADOOP_CONF_DIR`
in the file `conf/env.sh`.
