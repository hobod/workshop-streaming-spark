function FindProxyForURL (url, host) {
  if (shExpMatch(host, 'sber*.atp-fivt.org')) {
    return 'SOCKS5 localhost:8124'
  }
  if (shExpMatch(host, 'mipt*.atp-fivt.org')) {
    return 'SOCKS5 localhost:8124'
  }
  return 'DIRECT'
}

