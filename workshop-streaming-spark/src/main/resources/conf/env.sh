# Environment settings

export SPARK_HOME="/opt/spark/latest"

export HADOOP_CONF_DIR="/etc/hadoop/conf"

# Include Hadoop jars in the Spark classpath:
# https://spark.apache.org/docs/latest/hadoop-provided.html
SPARK_DIST_CLASSPATH="$(hadoop classpath)"
export SPARK_DIST_CLASSPATH
