#!/usr/bin/env bash

set -o nounset
set -o errexit
set -o pipefail

app_main_class="org.fivt.atp.bigdata.word_count.WordCountMain"

bin_dir=''
if echo "$0" | grep -q /
then
  # Started with absolute or relative path
  bin_dir="$(cd "$(dirname -- "$0")" ; pwd)"
else
  # Started from PATH
  bin_dir="$(cd "$(dirname -- "$(which "$0")")" ; pwd)"
fi

$bin_dir/app-submit.sh \
  -m $app_main_class
