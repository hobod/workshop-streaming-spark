#!/usr/bin/env bash

set -o nounset
set -o errexit
set -o pipefail

if echo "$0" | grep -q /
then
  # Started with absolute or relative path
  bin_dir="$(cd "$(dirname -- "$0")" ; pwd)"
else
  # Started from PATH
  bin_dir="$(cd "$(dirname -- "$(which "$0")")" ; pwd)"
fi
base_dir="$(dirname -- $bin_dir)"
libexec_dir="$base_dir/libexec"
lib_dir="$base_dir/lib"
conf_dir="$base_dir/conf"

source "$libexec_dir/lib-log.sh"
source "$conf_dir/env.sh"

function show_help() {
    cat <<END_HELP
Usage:
  $(basename "$0") -m <application main class>
END_HELP
}

while getopts ":m:h" opt
do
  case "$opt" in
    m)
      app_main_class="$OPTARG"
    ;;
    h)
      show_help
      exit 0
    ;;
    \?)
      log_err "Invalid option: -$OPTARG"
      show_help
      exit 1
    ;;
    :)
      log_err "Option -$OPTARG requires an argument."
      show_help
      exit 1
    ;;
  esac
done
if test $OPTIND -le "$#"
then
  log_err "Invalid options: $*"
  show_help
  exit 1
fi
if test -z "${app_main_class+x}"
then
  log_err "Missing mandatory argument"
  show_help
  exit 1
fi

log_info "Running main class: $app_main_class"

app_name="${app_main_class##*.}"
app_conf_file_name="app.conf"
app_conf_file="$conf_dir/$app_conf_file_name"
app_jar_name="workshop-streaming-spark-apps-@{project.version}.jar"
app_jar="$lib_dir/$app_jar_name"
spark_conf_file="$conf_dir/spark.conf"
log_conf_file_name="log4j.xml"
log_conf_file="$conf_dir/$log_conf_file_name"

jars=$(find "$lib_dir" -type f -name \*.jar -a ! -name "$app_jar_name")
spark_jars="${jars//[[:space:]]/,}"

# TODO Restore --verbose when fixed: https://issues.apache.org/jira/browse/SPARK-36804
"$SPARK_HOME/bin/spark-submit" \
  --properties-file "$spark_conf_file" \
  --jars "$spark_jars" \
  --class "$app_main_class" \
  --name "$app_name" \
  --master yarn \
  --deploy-mode cluster \
  --files "$app_conf_file,$log_conf_file" \
  --driver-java-options "-Dconfig.file=$app_conf_file_name
    -Dconfig.trace=loads
    -Dlog4j.configuration=$log_conf_file_name" \
  --conf spark.executor.extraJavaOptions="-Dlog4j.configuration=$log_conf_file_name" \
  "$app_jar"
