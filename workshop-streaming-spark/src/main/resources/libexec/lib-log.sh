
function timestamp() {
  date "+%Y-%m-%dT%H:%M:%S%z"
}

function log() {
    local msg="${*:-}"
    local ts_msg
    ts_msg="$(timestamp) $msg"
    echo "$ts_msg"
}

function log_info() {
  local msg="INFO: ${*:-}"
  log "$msg"
}

function log_warn() {
  local msg="WARN: ${*:-}"
  log "$msg" >&2
}

function log_err() {
  local msg="ERR: ${*:-}"
  log "$msg" >&2
}
